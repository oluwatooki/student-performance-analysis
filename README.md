# Comprehensive Analysis of Student Performance: Unveiling Patterns, Influencing Factors, and Test Preparation Efficiency

## Overview

This repository contains a data science project focused on analyzing student performance based on various factors such as parental education, test preparation, and gender. The project aims to understand the impact of test preparation on students' scores and identify which groups benefit the most from it.

## Project Structure

- **Notebooks**: This directory contains Jupyter notebooks for different stages of the analysis.
  - `01_student_performance_data_preparation.ipynb`: Initial preparation of the dataset to prepare it for data exploration and statistical analysis.
  - `02_exploratory_data_analysis.ipynb`: Exploratory data analysis of the dataset
  - `03_investigating_test_preparation_effectiveness.ipynb `: Investigating the impact of test preparation on student performance and which demographic best benefits from the course.
  - ...
- **Data**: This directory holds the datasets used in the project.
  - `student_performance.csv`: The raw dataset with student performance metrics.
  - `cleaned_students_performance.csv`: The main dataset with student performance metrics.
  - ...
- **Visualizations**: Contains visualizations generated during the analysis.
  - `effect_test_prep_parental_education.png`: Shows how the impact of completing the Test Preparation Course on student scores changes based on the different levels of Parental Education Level.
  -  `general_effect_test_prep.png"`: Shows the general effect of completing the Test Prep Course
  -  `lunch_type_effectiveness_test_prep.png`: Shows how the impact of the Test Prep Course is influenced by Lunch Type
  -  `distribution_of_categorical_variables.png`: Shows the distribution of categorical variables
  -  `distribution_of_scores.png`: Shows the distribution of scores for different subjects(math,writing,reading...)
  -  `general_effect_lunch_type.png` : Shows the general effect of lunch type on student performance
  -  `correlation_between_scores.png`: Shows the correlation between the various subject scores
  -  `general_effect_parental_education.png`: Shows the general effect of parental level of education on student performance
  -  `general_effect_gender.png`: Shows the general effect of gender on student performance
  - ...

## Dependencies

- Python 3.x
- Pandas
- Matplotlib
- Seaborn
- Pingouin
- Numpy

## Usage

- Follow the notebooks in sequential order for a comprehensive understanding of the analysis.

## Results

Soon
## License

This project is licensed under the [MIT License](LICENSE).
